/**
 * @file    Card.java
 * @author  CYRIL URBAN
 * @date    2017-03-01
 * @brief   The class Card
 */

package model.cards;

public class CardPack {

    public static Card[] cards = new Card[52];
    public int size = 0;
    public int top = 0;
    
        /**
     * remove (and read) the card from top of the stack
     *
     * @return Card popCard
     */
    public Card pop() {

        if (this.top == 0) {
            return null;
        } else {
            Card popCard = new Card(cards[top - 1].color(), cards[top - 1].value());
            this.top--;
            return popCard;
        }
    }
}
